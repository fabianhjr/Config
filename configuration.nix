{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  system = {
    stateVersion = "19.03";
    autoUpgrade.enable = true;
    autoUpgrade.channel = "https://nixos.org/channels/nixos-19.03/";
  };

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = false;
    };
    kernelPackages = pkgs.linuxPackages_latest;
    cleanTmpDir = true;
  };

  # Enable sound.
  sound.enable = true;
  hardware = {
    cpu.intel.updateMicrocode = true;
    pulseaudio = {
      enable  = true;
      package = pkgs.pulseaudioFull;
    };
  };

  environment = {
    systemPackages = with pkgs; [
      bat
      direnv
      exfat
      firefox
      fractal
      git
      gnupg
      jetbrains.idea-community
      keybase-gui
      mesa
      openssl
      python3
      recoll
      ripgrep
      ruby
      rustup
      scala
      shotcut
      stack
      tree
      unzip
      vlc
    ];
    gnome3.excludePackages = with pkgs; [
      gnome3.accerciser
      gnome3.adwaita-icon-theme
      gnome3.gnome-documents
    ];
  };

  security = {
    apparmor.enable = true;
  };

  services = {
    emacs = {
      enable = true;
      defaultEditor = true;
    };
    xserver = {
      enable = true;
      displayManager = {
        gdm.enable  = true;
        gdm.wayland = true;
      };
      desktopManager.gnome3.enable = true;
    };
    gnome3.chrome-gnome-shell.enable = true;
    keybase.enable = true;
    kbfs.enable = true;
  };

  programs = {
    adb.enable  = true;
    criu.enable = true;
    firejail = {
      enable = true;
      wrappedBinaries = with pkgs; {
        firefox = "${firefox}/bin/firefox";
        fractal = "${fractal}/bin/fractal";
      };
    };
    fish.enable = true;
    java = {
      enable = true;
      package = pkgs.openjdk11;
    };
  };

  users = {
    users.fabian = {
      isNormalUser = true;
      extraGroups  = [ "wheel" "adbusers" ];
      shell = pkgs.fish;
    };
  };

  nixpkgs.config = {
    allowUnfree = true;
    firefox.enableGnomeExtensions = true;
  };
}
