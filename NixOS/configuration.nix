{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  system = {
    stateVersion = "19.03";
    autoUpgrade.enable = true;
    autoUpgrade.channel = "https://nixos.org/channels/nixos-19.03/";
  };

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      systemd-boot.editor = false;
    };
    cleanTmpDir = true;
    kernelPackages = pkgs.linuxPackages_latest;
  };

  environment = {
    systemPackages = with pkgs; [
      bat
      cmake
      direnv
      firefox
      fractal
      git
      gnupg
      jetbrains.idea-community
      keybase-gui
      mesa
      openssl
      pciutils
      psmisc
      python3
      recoll
      ripgrep
      ruby
      rustup
      stack
      tree
      unzip
    ];
    gnome3.excludePackages = with pkgs; [
      gnome3.accerciser
      gnome3.adwaita-icon-theme
    ];
  };

  # Enable sound.
  sound.enable = true;
  hardware = {
    cpu.intel.updateMicrocode = true;
    pulseaudio = {
      enable  = true;
      package = pkgs.pulseaudioFull;
    };
  };

  security = {
    apparmor.enable = true;
  };

  services = {
    emacs = {
      enable = true;
      defaultEditor = true;
    };
    xserver = {
      enable = true;
      displayManager = {
        gdm.enable  = true;
        gdm.wayland = true;
      };
      desktopManager.gnome3.enable = true;
    };
    gnome3.chrome-gnome-shell.enable = true;
    keybase.enable = true;
    kbfs.enable = true;
  };

  programs = {
    adb.enable  = true;
    ccache = {
      enable = true;
      packageNames = [
        "linuxPackages_lates"
      ];
    };
    criu.enable = true;
    firejail = {
      enable = true;
      wrappedBinaries = {
      };
    };
    fish.enable = true;
    java.enable = true;
  };

  users = {
    users.fabian = {
      isNormalUser = true;
      extraGroups  = [ "wheel" "adbusers" ];
    };
    extraUsers.fabian = {
      shell = "/run/current-system/sw/bin/fish";
    };
  };

  nixpkgs.config = {
    allowUnfree = true;
    firefox.enableGnomeExtensions = true;
  };
}
